import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, ItemSliding } from 'ionic-angular';
import { Chart } from 'chart.js';
import { InversionModal } from './inversion-modal'
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    @ViewChild('doughnutCanvas') doughnutCanvas;

    doughnutChart : any;
    chartData : number[];
    porcentajePorAsignar : number;
    cantidadTotalInversion : number;
    restanteInversion: number;
    tablaInversiones : any[];
    listaColoresGrafica : any[];
    listaColoresGraficaHover : any[];
    comboTipoInversion : any[];
 
    constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
        this.cantidadTotalInversion = 5000;
        this.restanteInversion = 5000;
        this.porcentajePorAsignar = 100;
        this.chartData = [5000];
        this.tablaInversiones = [];
        this.listaColoresGrafica = [
                        'rgba(69,69,69,0.3)',
                        'rgba(46, 204, 113, 0.8)', //verde
                        'rgba(54, 162, 235, 0.8)', //azul 
                        'rgba(231, 76, 60, 0.8)', // rojo claro
                        'rgba(142, 68, 173, 0.8)', //morado wisteria
                        'rgba(211, 84, 0, 0.8)', //naranja pumpkin
                        'rgba(52, 73, 94, 0.8)', // wet asphalt
                        'rgba(30, 130, 76, 0.8)', //verde salem
                        'rgba(241, 196, 15, 0.8)', //amarillo
                        'rgba(150, 40, 27, 0.8)', //red brick
                        'rgba(78, 205, 196, 0.8)',//aqua
                        'rgba(58, 83, 155, 0.8)', //chambray
                        'rgba(219, 10, 91, 0.8)', //rosa 
                    ];
        this.listaColoresGraficaHover = [
                        'rgba(69,69,69,0.5)',
                        'rgba(46, 204, 113, 1)', //verde
                        'rgba(54, 162, 235, 1)', //azul 
                        'rgba(231, 76, 60, 1)', // rojo pussy
                        'rgba(142, 68, 173, 1)', //morado wisteria
                        'rgba(211, 84, 0, 1)', //naranja pumpkin
                        'rgba(52, 73, 94, 1)', // wet asphalt
                        'rgba(30, 130, 76, 1)', //verde salem
                        'rgba(241, 196, 15, 1)', //amarillo
                        'rgba(150, 40, 27, 1)', //red brick
                        'rgba(78, 205, 196, 1)', //aqua
                        'rgba(58, 83, 155, 1)', //chambray
                        'rgba(219, 10, 91, 1)', //rosa
                    ];
        this.comboTipoInversion = [
            {label:'Conservador - Dólares', value:'Conservador-Dólares', disabled:'false'},
            {label:'Conservador - Moneda Nacional', value:'Conservador-Moneda Nacional', disabled:'false'},
            {label:'Conservador - Euros', value:'Conservador-Euros', disabled:'false'},
            {label:'Balanceado - Dólares', value:'Balanceado-Dólares', disabled:'false'},
            {label:'Balanceado - Moneda Nacional', value:'Balanceado-Moneda Nacional', disabled:'false'},
            {label:'Balanceado - Euros', value:'Balanceado-Euros', disabled:'false'},
            {label:'Dinámico - Dólares', value:'Dinámico-Dólares', disabled:'false'},
            {label:'Dinámico - Moneda Nacional', value:'Dinámico-Moneda Nacional', disabled:'false'},
            {label:'Dinámico - Euros', value:'Dinámico-Euros', disabled:'false'}
        ];
    }
 
    ionViewDidLoad() {

        Chart.defaults.global.tooltips.enabled = false;
        //Chart.defaults.global.tooltips.titleFontSize = 0;
 
        this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
 
            type: 'doughnut',
            data: {
                datasets: [{
                    borderColor:this.listaColoresGrafica,
                    label: 'Mis inversiones',
                    data: this.chartData,
                    backgroundColor: this.listaColoresGrafica,
                    hoverBackgroundColor: this.listaColoresGraficaHover
                }]
            },
            options:{
                cutoutPercentage:'40'
            }
        });
 
    } 

    agregarInversion(){

        var porcentajePreview = this.restanteInversion*100/this.cantidadTotalInversion;

        var tipoInversionPreselect = "";

        for(var i=0 ; i<this.comboTipoInversion.length ; i++){
            if( this.comboTipoInversion[i].disabled === 'false' ){
                tipoInversionPreselect = this.comboTipoInversion[i].value;
                console.log("En if false comboTipo inv"+tipoInversionPreselect);
                
                break;
            }
        }

        // create modal
        let modal = this.modalCtrl.create(InversionModal, {
                                            porcentajeInversionValue : porcentajePreview,
                                            totalInversion: this.cantidadTotalInversion, 
                                            restanteInversion: this.restanteInversion, 
                                            tablaInversiones:this.tablaInversiones,
                                            esEdicion: 'false',
                                            comboTipoInversion : this.comboTipoInversion,
                                            tipoInversionPreselect : tipoInversionPreselect
                                        });
        // open modal

        modal.onDidDismiss( data => {

            if ( data!= null && data!= undefined ){

            var tipoInversionExiste = false;

            for(var index=0 ; index<this.tablaInversiones.length ; index++){
                if( data.tipoInversion === this.tablaInversiones[index].tipoInversion ){
                    tipoInversionExiste = true;

                    this.tablaInversiones[index].porcentajeInversion = Number(data.porcentajeInversion)+Number(this.tablaInversiones[index].porcentajeInversion);
                    this.tablaInversiones[index].cantidadInversion = Number(data.cantidadAsignada)+Number(this.tablaInversiones[index].cantidadInversion);
                    this.chartData[index+1] = this.tablaInversiones[index].cantidadInversion;
                }
            }

            if( !tipoInversionExiste ){

                //Iterar el combo de tipos de inversion para deshabilitar el tipo de inversion seleccionado

                for(var i=0 ; i<this.comboTipoInversion.length ; i++){

                    //console.log("Data tipo inversion "+data.tipoInversion);
                    //console.log("Combo tipo inversion "+this.comboTipoInversion[i].value);
                    if( data.tipoInversion === this.comboTipoInversion[i].value ){
                        //console.log("Tipo inversion iguales ");
                        this.comboTipoInversion[i].disabled = "true";
                    }
                }

                var rowInversion = {
                    color: this.listaColoresGrafica[this.tablaInversiones.length+1],
                    tipoInversion:data.tipoInversion,
                    porcentajeInversion: data.porcentajeInversion,
                    cantidadInversion: data.cantidadAsignada
                };

                this.tablaInversiones.push(rowInversion);
                this.chartData.push(data.cantidadAsignada);

            }

            this.restanteInversion = data.restanteInversion;  
            this.chartData[0] = this.restanteInversion;

            this.doughnutChart.data.datasets[0].data = this.chartData;
            this.doughnutChart.update();

            }          
        });

        modal.present();      
    }


    eliminarInversion(row){

        for(var i = 0; i < this.tablaInversiones.length; i++) {
 
            if(this.tablaInversiones[i] == row){

                var cantidadSuma = this.tablaInversiones[i].porcentajeInversion*this.cantidadTotalInversion/100;
                this.restanteInversion += cantidadSuma;
                this.chartData[0] = this.restanteInversion;
                var colorTmp = this.listaColoresGrafica[i+1];
                var colorHoverTmp = this.listaColoresGraficaHover[i+1];

                this.chartData.splice(i+1,1);
                this.listaColoresGrafica.splice(i+1,1);
                this.listaColoresGraficaHover.splice(i+1,1);

                this.listaColoresGrafica.push(colorTmp);
                this.listaColoresGraficaHover.push(colorHoverTmp);

                this.doughnutChart.update();

                this.tablaInversiones.splice(i, 1);

                for(var j=0 ; j<this.comboTipoInversion.length ; j++){
                    if( this.comboTipoInversion[j].value ===  row.tipoInversion){
                        console.log("En if eliminar info ");
                        this.comboTipoInversion[j].disabled = "false";
                    }
                }
            }
        
        }
    }


    editarInversion(row, slidingItem: ItemSliding){
        console.log("Tipo de inversion row "+row.tipoInversion);
        //console.log("Porcentaje inversion row "+row.porcentajeInversion);

        let modal = this.modalCtrl.create(InversionModal, {
                                            porcentajeInversionValue : row.porcentajeInversion,
                                            totalInversion: this.cantidadTotalInversion, 
                                            restanteInversion: this.restanteInversion, 
                                            tablaInversiones:this.tablaInversiones,
                                            filaEdit: row,
                                            esEdicion: 'true',
                                            tipoInversionPreselect: row.tipoInversion,
                                            comboTipoInversion : this.comboTipoInversion,
                                        });
        
        modal.onDidDismiss( data => {

            console.log("On dismiss edit");

            if ( data!= null && data!= undefined ){

                console.log("Restante inversion on dismiss "+data.restanteInversion);
                console.log("Porcentaje on dismiss "+data.porcentajeReasignado);
                console.log("cantidad on dismiss "+data.cantidadReasignada);
                console.log("Fila on dismiss "+data.filaEdit);

                for(var i = 0; i < this.tablaInversiones.length; i++) {
 
                    if(this.tablaInversiones[i] == data.filaEdit){
                        console.log("Fila igual");

                        this.tablaInversiones[i].porcentajeInversion = data.porcentajeReasignado;
                        this.tablaInversiones[i].cantidadInversion = data.cantidadReasignada;

                        this.restanteInversion = data.restanteInversion;
                        this.chartData[0] = this.restanteInversion;
                        this.chartData[i+1] = this.tablaInversiones[i].cantidadInversion;

                        this.doughnutChart.update();

                    }

                }
                
            }

            slidingItem.close();
                    
        });

        modal.present();

    }
 
}



