import { Component} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';


@Component({
    templateUrl: 'inversion-modal.html'
})
export class InversionModal {

   
  tipoInversion : string;
  porcentajeInversionValue : number;
  totalInversion : number;
  restanteInversion : number;
  inversionValida : boolean;
  inversionBanner : boolean;
  tablaInversiones : any[];
  disableTipoInversion: boolean;
  esEdicion: string;
  rowEdit: any;
  comboTipoInversion: any[];


  constructor(private viewCtrl: ViewController, private params: NavParams) {
    this.tipoInversion = params.get('tipoInversionPreselect') || "Conservador-Dólares";
    console.log("Tipo Inversion preselect "+this.tipoInversion);
    this.porcentajeInversionValue = params.get('porcentajeInversionValue');
    this.inversionValida = true;
    this.inversionBanner = true;
    this.totalInversion = params.get('totalInversion');
    this.restanteInversion = params.get('restanteInversion');
    this.tablaInversiones = params.get('tablaInversiones');
    this.disableTipoInversion = params.get('esEdicion') || false;
    this.esEdicion = params.get('esEdicion');
    this.rowEdit = params.get('filaEdit') || null;
    this.comboTipoInversion = params.get('comboTipoInversion');
  }

  cancelar() {
    this.viewCtrl.dismiss();
  }

  agregarInversion(tipoInversion, porcentajeInversion){

    if( this.esEdicion === 'true' ){
      console.log("En agregar inversion es edicion");


      for(var index=0 ; index<this.tablaInversiones.length ; index++){
          
            if(this.rowEdit===this.tablaInversiones[index]){

              console.log("Es igual row agregar");
              //var tipoInversionExiste = true;
              
              //comparar porcentaje recibido contra actual

                //si porcentaje < 

                var cantidadAsignada = porcentajeInversion*this.totalInversion/100;

                if( Number(porcentajeInversion) > this.tablaInversiones[index].porcentajeInversion ){
                    console.log("Es mayor ");

                    //var cantidadAsignada = porcentajeInversion*this.totalInversion/100;
                    var cantidadTmp = cantidadAsignada-this.tablaInversiones[index].cantidadInversion;
                    this.restanteInversion-=cantidadTmp;


                    //regresar restante, fila y porcentaje

                    /*
                    var cantidadParcial = porcentajeInversion*this.totalInversion/100;
                    cantidadTmp = cantidadParcial-this.tablaInversiones[index].cantidadInversion;
                    restanteTmp = this.restanteInversion-cantidadTmp;

                    console.log("Cantidad Parcial "+cantidadParcial);
                    console.log("Cantidad tmp "+cantidadTmp);
                    console.log("Restante tmp "+restanteTmp);*/

                }else if( Number(porcentajeInversion) < this.tablaInversiones[index].porcentajeInversion ){
                    console.log("Es menor o igual");

                    var cantidadTmp = this.tablaInversiones[index].cantidadInversion-cantidadAsignada;
                    this.restanteInversion+=cantidadTmp;

                    /*
                    var cantidadParcial = porcentajeInversion*this.totalInversion/100;
                    cantidadTmp = this.tablaInversiones[index].cantidadInversion-cantidadParcial;
                    restanteTmp = this.restanteInversion+cantidadTmp;

                    console.log("Cantidad Parcial "+cantidadParcial);
                    console.log("Cantidad tmp "+cantidadTmp);
                    console.log("Restante tmp "+restanteTmp);*/
                }


                 this.viewCtrl.dismiss({
                                  restanteInversion : this.restanteInversion,
                                  porcentajeReasignado : porcentajeInversion,
                                  cantidadReasignada : cantidadAsignada,
                                  filaEdit : this.rowEdit
                  });     
            }
          }
    
    }else{
  
      console.log("En agregar inversion no es edicion");

      var cantidadAsignada = porcentajeInversion*this.totalInversion/100;
      this.restanteInversion -= cantidadAsignada;

      this.viewCtrl.dismiss({
        cantidadAsignada:cantidadAsignada,
        porcentajeInversion:porcentajeInversion,
        tipoInversion:tipoInversion,
        restanteInversion:this.restanteInversion
      });

    }

  }

  
  porcentajeOnChange(porcentajeInversion, tipoInversionModal){

    //validar si el tipo de inversion ya existe

      var tipoInversionExiste = false;
      var cantidadTmp = 0;
      var restanteTmp = 0;

      console.log("Es edicion "+this.esEdicion);



      if( this.esEdicion === 'true' ){

          console.log("En if es edicion");  


          console.log("Fila edit "+this.rowEdit.tipoInversion);
          console.log("Fila edit "+this.rowEdit.porcentajeInversion);
          

          for(var index=0 ; index<this.tablaInversiones.length ; index++){
          
            if(this.rowEdit===this.tablaInversiones[index]){

              console.log("Es igual row");
              //var tipoInversionExiste = true;
              
              //comparar porcentaje recibido contra actual

                //si porcentaje < 

                var cantidadParcial = porcentajeInversion*this.totalInversion/100;

                if( Number(porcentajeInversion) > this.tablaInversiones[index].porcentajeInversion ){
                    console.log("Es mayor ");

                    var cantidadParcial = porcentajeInversion*this.totalInversion/100;
                    cantidadTmp = cantidadParcial-this.tablaInversiones[index].cantidadInversion;
                    restanteTmp = this.restanteInversion-cantidadTmp;

                    console.log("Cantidad Parcial "+cantidadParcial);
                    console.log("Cantidad tmp "+cantidadTmp);
                    console.log("Restante tmp "+restanteTmp);

                }else if( Number(porcentajeInversion) < this.tablaInversiones[index].porcentajeInversion ){
                    console.log("Es menor o igual");

                    var cantidadParcial = porcentajeInversion*this.totalInversion/100;
                    cantidadTmp = this.tablaInversiones[index].cantidadInversion-cantidadParcial;
                    restanteTmp = this.restanteInversion+cantidadTmp;

                    console.log("Cantidad Parcial "+cantidadParcial);
                    console.log("Cantidad tmp "+cantidadTmp);
                    console.log("Restante tmp "+restanteTmp);
                }

                if( cantidadParcial >=1000 ){
                  console.log("Cantidad tmp >=1000 ");
                  if( restanteTmp>=1000 || restanteTmp==0){
                    console.log("DEntro de if***");
                    this.inversionValida = true;
                    this.inversionBanner = true;
                  }else{
                    console.log("Dentro de if 1");
                    this.inversionValida = false;
                    this.inversionBanner = false;
                  }
                }else{
                    console.log("Dentro de if 2");
                    this.inversionValida = false;
                    this.inversionBanner = false;
                }
            }
          }

      }else{

          console.log("En else es edicion"); 

          for(var index=0 ; index<this.tablaInversiones.length ; index++){
          
            if(tipoInversionModal===this.tablaInversiones[index].tipoInversion){
              var tipoInversionExiste = true;
              
              //comparar porcentaje recibido contra actual

                //si porcentaje < 

              var cantidadParcial = porcentajeInversion*this.totalInversion/100;
              cantidadTmp = cantidadParcial + this.tablaInversiones[index].cantidadInversion;
              restanteTmp = this.restanteInversion-cantidadParcial;

            }
          }

          if(!tipoInversionExiste){
            cantidadTmp = porcentajeInversion*this.totalInversion/100;
            restanteTmp = this.restanteInversion-cantidadTmp;
          }


          if( cantidadTmp >=1000 ){

            if( restanteTmp>=1000 || restanteTmp==0){
              this.inversionValida = true;
              this.inversionBanner = true;
            }else{
              this.inversionValida = false;
              this.inversionBanner = false;
            }
          }else{
            this.inversionValida = false;
            this.inversionBanner = false;
          }


      }
      
  }

}